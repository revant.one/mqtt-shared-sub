/* eslint no-console: "off" */
/* eslint @typescript-eslint/no-var-requires: "off" */

const mqtt = require('mqtt');

const mqttUri = process.env.MQTT_URI || 'mqtt://user:changeit@localhost:1883';

console.info("Connecting to : " + mqttUri);

const client = mqtt.connect(mqttUri, { protocolVersion: 5 });

const quitMessage = 'Ctrl + C to quit';
client.on('connect', () => {
  console.info(`MQTT client connected. ${quitMessage}`);
});

const topicPattern = process.env.EVENT_PATTERN || '$queue/+';
client.subscribe(topicPattern, { qos: 1 });

client.on('message', (topic, message) => {
  console.info(`Topic: ${topic}`);
  console.info(message.toString());
  console.info(quitMessage);
  console.info();
});
