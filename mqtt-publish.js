/* eslint no-console: "off" */
/* eslint @typescript-eslint/no-var-requires: "off" */

const mqtt = require('mqtt');

let eventPattern = process.env.EVENT_PATTERN;

if (!eventPattern) {
  eventPattern = 'MessageEmitted';
  console.warn(
    `EVENTS_PATTERN env variable not set. Using pattern: ${eventPattern}`,
  );
}
const mqttUri = process.env.MQTT_URI || 'mqtt://user:changeit@localhost:1883';

console.info("Connecting to : " + mqttUri);

const client = mqtt.connect(mqttUri, { protocolVersion: 5 });

let payload = {};
try {
  payload = JSON.parse(process.env.PAYLOAD)
} catch {
  payload = { message: 'ping', id: Math.random().toString().split('.').pop() };
}

console.info(
  `Using payload: ${JSON.stringify(payload)} and pattern : ${eventPattern}`,
);

client.on('connect', () => {
  client.subscribe(eventPattern, err => {
    if (!err) {
      client.publish(eventPattern, JSON.stringify(payload));
    }
    client.end();
  });
});
