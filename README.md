### Install dependencies

```shell
yarn
```

or

```shell
npm i
```

### Run subscribers in multiple terminals

```shell
node mqtt-subscribe.js
```

### Publish message

```shell
node mqtt-publish.js
```

### Sample Output

![Example](assets/mqtt-shared-subscription.png?raw=true)
